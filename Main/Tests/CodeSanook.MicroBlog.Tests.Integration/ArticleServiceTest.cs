﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using CodeSanook.MicroBlog.Tests.Unit.Builders;
using FluentAssertions;
using NUnit.Framework;

namespace CodeSanook.MicroBlog.Tests.Integration
{
    public class ArticleServiceTest : IntegrationTestFixtureBase
    {
        private ArticleService _articleService;
        private IArticleRepository _articleRepository;
        private IUserRepository _userRepository;

        public override void TestFixtureSetup()
        {
            base.TestFixtureSetup();

            _articleRepository = Scope.Resolve<IArticleRepository>();
            _articleService = Scope.Resolve<ArticleService>();
            _userRepository = Scope.Resolve<IUserRepository>();
        }

        [Test]
        public void CreateArticle_ValidInput_ArticleCountEqual1()
        {
            User author;
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                author = new UserBuilder()
                    .Create("theeranitp@gmail.com", "1234")
                    .Build();
                _userRepository.Add(author);
                unitOfWork.Commit();
            }

            var article = new ArticleBuilder()
                .Create(author,
                        "CodeSanook new release",
                        "codesanook-new-release",
                        "details",
                        "summary",
                        "caption.jpg",
                        DateTime.UtcNow,
                        true)
                .Build();
            _articleService.CreateArticle(article);

            using (UnitOfWorkFactory.Create())
            {
                _articleRepository.FindAll().Count().Should().Be(1);
                author = _userRepository.FindById(author.Id);
                author.Should().NotBe(null);
            }

        }

        [Test]
        public void GetArticleList_1ArticleExist_1ArticleReturn()
        {
            User author;
            Article article;
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                author = new UserBuilder()
                    .Create("theeranitp@gmail.com", "1234")
                    .Build();
                _userRepository.Add(author);

                article = new ArticleBuilder()
                    .Create(author,
                            "CodeSanook new release",
                            "codesanook-new-release",
                            "details",
                            "summary",
                            "caption.jpg",
                            DateTime.UtcNow,
                            true)
                    .Build();

                _articleRepository.Add(article);

                unitOfWork.Commit();
            }


            var pageNo = 1;
            var itemPerPage = 5;
            var response = _articleService.GetArticleList(pageNo, itemPerPage);
            response.IsSuccess.Should().BeTrue();
            response.Content.ArticleList.Count().Should().Be(1);
            response.Content.NumberOfArticles.Should().Be(1);
        }

        [Test]
        public void GetArticleDetails_1ArticleExist_1ArticleReturn()
        {

            User author;
            Article article;
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                author = new UserBuilder()
                    .Create("theeranitp@gmail.com", "1234")
                    .Build();
                _userRepository.Add(author);

                article = new ArticleBuilder()
                    .Create(author,
                            "CodeSanook new release",
                            "codesanook-new-release",
                            "details",
                            "summary",
                            "caption.jpg",
                            DateTime.UtcNow,
                            true)
                    .Build();

                _articleRepository.Add(article);

                unitOfWork.Commit();
            }

            var response = _articleService.GetArticleDetail(article.Alias);
            response.IsSuccess.Should().BeTrue();
            response.Content.Id.Should().Be(1);
            response.Content.Alias.Should().Be(article.Alias);
        }

        [Test]
        public void DeleteArticle_1ArticleExist_NoArticleLeft()
        {
            User author;
            Article article;
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                author = new UserBuilder()
                    .Create("theeranitp@gmail.com", "1234")
                    .Build();
                _userRepository.Add(author);

                article = new ArticleBuilder()
                    .Create(author,
                            "CodeSanook new release",
                            "codesanook-new-release",
                            "details",
                            "summary",
                            "caption.jpg",
                            DateTime.UtcNow,
                            true)
                    .Build();

                _articleRepository.Add(article);

                unitOfWork.Commit();
            }

            var response = _articleService.DeleteArticle(article.Id);
            response.IsSuccess.Should().BeTrue();
            using (UnitOfWorkFactory.Create())
            {
                _articleRepository.FindAll().Count().Should().Be(0);
            }
        }

        //[Test]
        //public void CreateArticle_ValidListOfId_GetByIdListReturn()
        //{
        //    User author;
        //    using (var unitOfWork = UnitOfWorkFactory.Create())
        //    {
        //        author = new UserBuilder()
        //            .Create("theeranitp@gmail.com", "1234")
        //            .Build();
        //        _userRepository.Add(author);

        //        unitOfWork.Commit();
        //    }

        //    var article = new ArticleBuilder()
        //        .Create(author,
        //                "CodeSanook new release",
        //                "codesanook-new-release",
        //                "details",
        //                "summary",
        //                "caption.jpg",
        //                DateTime.UtcNow,
        //                true)
        //        .Build();

        //    article.Id = 1;
        //    article.Title = "title";

        //    _articleService.CreateArticle(article);




        //    using (UnitOfWorkFactory.Create())
        //    {
        //        article = _articleRepository.GetBy<Article>(1,"title");
        //        article.Id.Should().Be(1);
        //    }

        //}

        //[Test]
        //public void CreateArticle_ValidListOfId_GetByCompositeKeyIdListReturn()
        //{
        //    User author;
        //    using (var unitOfWork = UnitOfWorkFactory.Create())
        //    {
        //        author = new UserBuilder()
        //            .Create("theeranitp@gmail.com", "1234")
        //            .Build();
        //        _userRepository.Add(author);

        //        unitOfWork.Commit();
        //    }

        //    var article = new ArticleBuilder()
        //        .Create(author,
        //                "CodeSanook new release",
        //                "codesanook-new-release",
        //                "details",
        //                "summary",
        //                "caption.jpg",
        //                DateTime.UtcNow,
        //                true)
        //        .Build();
        //    _articleService.CreateArticle(article);

        //    using (UnitOfWorkFactory.Create())
        //    {
        //        var expectedId = 1;
        //        var primaryKeys = new Dictionary<string, object>();
        //        primaryKeys.Add("Id", expectedId);

        //        var articles = _articleRepository.GetBy<Article>(primaryKeys);
        //        articles.Id.Should().Be(expectedId);


        //        using (UnitOfWorkFactory.Create())
        //        {
        //            article = _articleRepository.GetBy<Article>(1);
        //            article.Id.Should().Be(1);
        //        }
        //    }

        //}//end method

    }

}
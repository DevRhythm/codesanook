﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using CodeSanook.MicroBlog.Tests.Unit.Builders;
using FluentAssertions;
using NUnit.Framework;
using CodeSanook.MicroBlog.Repositories.NH;

namespace CodeSanook.MicroBlog.Tests.Integration
{
    public class TbMasCancellationTestt : IntegrationTestFixtureBase
    {
        public override void TestFixtureSetup()
        {
            base.TestFixtureSetup();
        }

        [Test]
        public void GetObject()
        {
            const byte cultureId = 1;
            TbMasCancellation tbMasCancellation;
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                var session = SessionFactory.GetCurrentSession();


                tbMasCancellation = new TbMasCancellation()
                {
                    CultureId = cultureId,
                };
                session.Save(tbMasCancellation);
                unitOfWork.Commit();
            }


            TbMasCancellationInfo tbMasCancellationInfo;
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                var session = SessionFactory.GetCurrentSession();
                tbMasCancellationInfo = new TbMasCancellationInfo()
                {
                    Key = new TbMasCancellationInfoKey()
                    {
                        CancelId = tbMasCancellation.CancellationId,
                        CultureId = cultureId,
                    },
                    Cancellation = tbMasCancellation
                };

                tbMasCancellation.CancellationInfos.Add(tbMasCancellationInfo);
                session.Save(tbMasCancellationInfo);
                unitOfWork.Commit();
            }


            var key = new TbMasCancellationInfoKey()
            {
                CultureId = cultureId,
                CancelId = tbMasCancellation.CancellationId
            };
            using (UnitOfWorkFactory.Create())
            {

                var session = SessionFactory.GetCurrentSession();
                var newTbMasCancellationInfo = session.Get<TbMasCancellationInfo>(key);

                newTbMasCancellationInfo.Key
                    .CancelId.Should().Be(tbMasCancellation.CancellationId);
                newTbMasCancellationInfo.Key
                    .CultureId.Should().Be(cultureId);
            }
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using CodeSanook.MicroBlog.Tests.Unit.Builders;
using FluentAssertions;
using NUnit.Framework;

namespace CodeSanook.MicroBlog.Tests.Integration
{
    public class GuidCombGenerateTest : IntegrationTestFixtureBase
    {
        private IUserRepository _userRepository;

        public override void TestFixtureSetup()
        {
            base.TestFixtureSetup();
            _userRepository = Scope.Resolve<IUserRepository>();
        }

        [Test]
        public void GenerateGuidCombTest()
        {
            User author =null;
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                for (var i = 0; i < 10; i++)
                {
                    author = new UserBuilder()
                        .Create("theeranitp@gmail.com", "1234")
                        .Build();
                    _userRepository.Add(author);
                    Log.DebugFormat("generated author Guid.Comb: {0}", author.Id);
                }

                unitOfWork.Commit();

            }

            
        }

    }

}
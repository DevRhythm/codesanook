
CREATE CLUSTERED INDEX [RolesToUsers_UserId_RoleId] ON [dbo].[RolesToUsers] 
(
	[RoleId] ASC,
	[UserId] ASC
)
GO


CREATE CLUSTERED INDEX [ArticlesToTags_ArticleId_TagId] ON [dbo].[ArticlesToTags] 
(
	[ArticleId] ASC,
	[TagId] ASC
)
GO

﻿using System;
using CodeSanook.MicroBlog.Infrastructure;
using CodeSanook.MicroBlog.Model;

namespace CodeSanook.MicroBlog.Tests.Unit.Builders
{
    public class ArticleBuilder : BuilderBase<ArticleBuilder, Article>
    {
        private string _alias;
        private User _author;
        private string _title;
        private string _detail;
        private string _abstract;
        private string _previewImageUrl;
        private DateTime _utcRelaseDate;
        private bool _isPublish;

        public override ArticleBuilder Create()
        {
            return this;
        }

        public ArticleBuilder Create(
            User author,
            string title,
            string alias,
            string detail,
            string @abstract,
            string previewImageUrl,
            DateTime utcRelaseDate,
            bool isPublish)
            
        {
            _author = author;
            _title = title;
            _alias = alias;
            _detail = detail;
            _abstract = @abstract;
            _previewImageUrl = previewImageUrl;
            _utcRelaseDate = utcRelaseDate;
            _isPublish = isPublish;
            return this;
        }




        public override Article Build()
        {
            var article = new Article()
            {
                Author = _author,
                Title = _title,
                Alias = _alias,
                Abstract = _abstract,
                Details = _detail,
                PreviewImageUrl = _previewImageUrl,
                UtcReleaseDate = _utcRelaseDate,
                IsPublish = _isPublish,
                
            };

            return article;
        }

        public override void Clear()
        {
            throw new System.NotImplementedException();
        }

        public ArticleBuilder WithAlias(string alias)
        {
            _alias = alias;
            return this;
        }
    }
}
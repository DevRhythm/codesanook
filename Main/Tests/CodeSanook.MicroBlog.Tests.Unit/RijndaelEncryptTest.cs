﻿using System;
using CodeSanook.Utility;
using NUnit.Framework;

namespace CodeSanook.MicroBlog.Tests.Unit
{
    public class RinjdaelEncryptTest
    {


        /// <summary>
        /// test to see if encrypted text and decrypted text are equal
        /// for use in authen cookie
        /// </summary>
        [Test]
        public void EncryptedDecryptedTextEqual()
        {
            var membership = "embarus, 12345";

            var encryptedText = RijndaelEncrypt.Encrypt(membership, true);
            Console.WriteLine(encryptedText);
            var decryptedText = RijndaelEncrypt.Decrypt(encryptedText, true);


            Assert.AreEqual(membership, decryptedText);
            var memberships = decryptedText.Split(',');
            Console.WriteLine("userName = {0}", memberships[0]);
            Console.WriteLine("password = {0}", memberships[1]);
        }


    }
}

﻿using System.Web.Mvc;
using System.Web.Security;

namespace CodeSanook.Utility
{
    public class AjaxAuthorizeAttribute : AuthorizeAttribute
    {

        private readonly AjaxAuthorizeAction action;
        public AjaxAuthorizeAttribute():this(AjaxAuthorizeAction.Redirect)
        {

        }

        public AjaxAuthorizeAttribute(AjaxAuthorizeAction action)
        {
            this.action = action;
        }


        /// <summary>
        /// update authen request
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {

            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                if (!base.AuthorizeCore(filterContext.HttpContext))
                {
                  switch(action)
                  {
                      case AjaxAuthorizeAction.Redirect:

                          filterContext.HttpContext.Response.ContentType = "application/x-javascript";
                          var loginUrl = UrlHelper.GenerateContentUrl(FormsAuthentication.LoginUrl,
                                                                      filterContext.HttpContext);
                          var script = string.Format("window.location='{0}';", loginUrl);
                          filterContext.HttpContext.Response.Write(script);

                          filterContext.HttpContext.Response.End();
                          filterContext.HttpContext.Response.Close();
                          break;
                      case AjaxAuthorizeAction.ReturnResult:
                          filterContext.HttpContext.Response.ContentType = "application/json";

                          filterContext.HttpContext.Response.Write(
                              string.Format("{{IsAuthenticated:{0}}}", false));

                          filterContext.HttpContext.Response.End();
                          filterContext.HttpContext.Response.Close();
                     
                          break;
                  }
                }
            }
            else
            {
                base.OnAuthorization(filterContext);


            }


        }




    }


    public enum AjaxAuthorizeAction
    {
        Redirect,
        ReturnResult,

    }

}
﻿using System.Collections.Generic;
using System.Linq;

namespace CodeSanook.Utility
{
    public static class EnumerableHelper
    {
        public static IEnumerable<t> TakeForPagination<t>
            (this IEnumerable<t> ienumerable, int pageNo, int itemPerPage)
        {
            return ienumerable.Skip((pageNo - 1) * itemPerPage)
                     .Take(itemPerPage)
                     .AsEnumerable();
        }
    }
}

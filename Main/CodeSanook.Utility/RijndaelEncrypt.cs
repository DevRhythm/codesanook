﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace CodeSanook.Utility
{

    /// <summary>
    /// This class uses a symmetric key algorithm (Rijndael/AES) to encrypt and 
    /// decrypt data. As long as encryption and decryption routines use the same
    /// parameters to generate the keys, the data is guaranteed to be the same.
    /// The class uses static functions with duplicate code to make it easier to
    /// demonstrate encryption and decryption logic. In a real-life application, 
    /// this may not be the most efficient way of handling encryption.
    /// 
    /// K Cartlidge
    /// </summary>
    public class RijndaelEncrypt
    {
        #region Settings
        private static bool UseUTF8 = true;
        private static string passPhrase = "uphencrypt";           // can be any string
        private static string saltValue = "protectme";             // can be any string
        private static string hashAlgorithm = "SHA1";              // can also be "MD5"
        private static string initVector = "4567456871234568";     // should be 16 characters

        private static int passwordIterations = 2;                 // can be any number
        private static int keySize = 256;                          // can also be 192 or 128
        #endregion
        #region Private Methods
        /// <summary>Encrypts specified plaintext using Rijndael symmetric key algorithm and returns a base64-encoded result.</summary>
        /// <param name="plainText">Plaintext value to be encrypted.</param>
        /// <param name="passPhrase">Passphrase from which a pseudo-random password will be derived. The
        /// derived password will be used to generate the encryption key. Passphrase can be any string.
        /// In this example we assume that this passphrase is an ASCII string.</param>
        /// <param name="saltValue">Salt value used along with passphrase to generate password. Salt can
        /// be any string. In this example we assume that salt is an ASCII string.</param>
        /// <param name="hashAlgorithm">Hash algorithm used to generate password. Allowed values are: "MD5" and
        /// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.</param>
        /// <param name="passwordIterations">Number of iterations used to generate password. 1 or 2 should be enough.</param>
        /// <param name="initVector">Initialization vector (or IV). This value is required to encrypt the
        /// first block of plaintext data. For RijndaelManaged class IV must be exactly 16 ASCII characters long.</param>
        /// <param name="keySize">Size of encryption key in bits. Allowed values are: 128, 192, and 256.
        /// Longer keys are more secure than shorter keys.</param>
        /// <returns>Encrypted value formatted as a base64-encoded string.</returns>
        private static string _Encrypt(string plainText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations,
                                     string initVector, int keySize)
        {
            // Convert strings into byte arrays; assumes that vector strings etc are ASCII, else change encoding.
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            // Convert our plaintext into a byte array
            byte[] plainTextBytes;
            plainTextBytes = (UseUTF8 ? Encoding.UTF8.GetBytes(plainText) : Encoding.ASCII.GetBytes(plainText));

            // Create a password, from which the key will be derived.
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);

            // Use the password to generate pseudo-random bytes for the encryption key. Specify the size of the key in bytes (instead of bits).
            byte[] keyBytes = password.GetBytes(keySize / 8);

            // Create uninitialized Rijndael encryption object and encryptor.
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);

            // Define streams for encrypted data and cryptographic stream.
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);

            // Encrypt.
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();

            // Convert encrypted data from a memory stream into a byte array, then close streams.
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();

            // Convert encrypted data into a base64-encoded string and return.
            string cipherText = Convert.ToBase64String(cipherTextBytes);
            return cipherText;
        }

        /// <summary>Decrypts specified ciphertext using Rijndael symmetric key algorithm.</summary>
        /// <param name="cipherText">Base64-formatted ciphertext value.</param>
        /// <param name="passPhrase">Passphrase from which a pseudo-random password will be derived. The
        /// derived password will be used to generate the encryption key. Passphrase can be any string, but we assume that this
        /// passphrase is an ASCII string.</param>
        /// <param name="saltValue">Salt value used along with passphrase to generate password. Salt can
        /// be any string, but we assume that salt is an ASCII string.</param>
        /// <param name="hashAlgorithm">Hash algorithm used to generate password. Allowed values are: "MD5" and
        /// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.</param>
        /// <param name="passwordIterations">Number of iterations used to generate password. One or two iterations
        /// should be enough.</param>
        /// <param name="initVector">Initialization vector (or IV). This value is required to encrypt the
        /// first block of plaintext data. For RijndaelManaged class IV must be exactly 16 ASCII characters long.</param>
        /// <param name="keySize">Size of encryption key in bits. Allowed values are: 128, 192, and 256.
        /// Longer keys are more secure than shorter keys.</param>
        /// <returns>Decrypted string value.</returns>
        /// <remarks>Most of the logic in this function is similar to the Encrypt
        /// version. In order for decryption to work, all parameters of this function
        /// - except cipherText value obviously - must match the corresponding parameters of
        /// the Encrypt function which was called to generate the ciphertext originally.</remarks>
        private static string _Decrypt(string cipherText, string passPhrase, string saltValue, string hashAlgorithm,
                                     int passwordIterations, string initVector, int keySize)
        {
            // Convert strings into byte arrays; assumes only ASCII, else change encoding.
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

            // Convert our ciphertext into a byte array.
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

            // Create a password, from which the key will be derived.
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);

            // Use the password to generate pseudo-random bytes for the encryption key. Specify the size of the key in bytes (instead of bits).
            byte[] keyBytes = password.GetBytes(keySize / 8);

            // Create uninitialized Rijndael encryption object and decryptor.
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);

            // Define streams for encrypted data and cryptographic stream.
            MemoryStream memoryStream = new MemoryStream(cipherTextBytes);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

            // Plaintext is never longer than ciphertext, so set buffer accordingly.
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            // Decrypt and close streams.
            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();

            // Convert decrypted data into a string.
            string plainText;
            plainText = (UseUTF8 ? Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount) : Encoding.ASCII.GetString(plainTextBytes, 0, decryptedByteCount));
            return plainText;
        }
        #endregion

        #region Public Methods
        /// <summary>Encrypts text into base-64 using Rijndael</summary>
        /// <param name="OriginalText">Original plain text string</param>
        /// <param name="IsUTF8">If False, plain text is treated as ASCII</param>
        /// <returns>Encrypted value formatted as a base64-encoded string</returns>
        public static string Encrypt(string OriginalText, bool IsUTF8)
        {
            UseUTF8 = IsUTF8;
            return _Encrypt(OriginalText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
        }
        /// <summary>Decrypts text from base-64 using Rijndael</summary>
        /// <param name="EncryptedText">Encrypted value formatted as a base64-encoded string</param>
        /// <param name="IsUTF8">If False, plain text is treated as ASCII</param>
        /// <returns>Original plain text string</returns>
        /// <remarks>This method uses the predefined hash etc and so will only function
        /// with text encrypted via the encrypt method with the same settings</remarks>
        public static string Decrypt(string EncryptedText, bool IsUTF8)
        {
            UseUTF8 = IsUTF8;
            return _Decrypt(EncryptedText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
        }
        #endregion
    }

}

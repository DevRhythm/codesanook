﻿using System;
using System.Text;

namespace CodeSanook.Utility
{
   public class RandomUtility
    {
        /// <summary>
        /// Randoms with character with number
        /// </summary>
        /// <param name="length">The length of rundom</param>
        /// <returns></returns>
       public static string RandomCharacter(int length)
        {
            /// Create an array of characters to user for password reset.
            /// Exclude confusing or ambiguous characters such as 1 0 l o i
            var characters = new string[] { "2", "3", "4", "5", "6", "7", "8",
                "9", "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n",
                "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

            var newPassword = new StringBuilder();
            var rnd = new Random();

            for (int i = 0; i < length; i++)
            {
                newPassword.Append(characters[rnd.Next(characters.GetUpperBound(0))]);
            }

            return newPassword.ToString();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CodeSanook.MicroBlog.Infrastructure;

//loader.io
namespace CodeSanook.MicroBlog.Model
{
    /// <summary>
    /// model for Article
    /// </summary>
    public class Article : IAggregateRoot, IUtcCreateDate, IUtcLastUpdate
    {
        public virtual int Id { get; set; }
        public virtual User Author { get; set; }

        [Display(Name = "ชื่อ")]
        [Required(ErrorMessage = "กรุณากรอกชื่อ")]
        public virtual string Title { get; set; }

        [Display(Name = "นามแฝง (url friendly")]
        [Required(ErrorMessage = "กรุณากรอกนามแฝง")]
        public virtual string Alias { get; set; }

        [Display(Name = "รายละเอียด")]
        [Required(ErrorMessage = "กรุณากรอกรายละเอียด")]
        public virtual string Details { get; set; }

        [Display(Name = "สรุปเนื้อหา")]
        [Required(ErrorMessage = "กรุณากรอกสรุปเนื้อหา")]
        public virtual string Abstract { get; set; }

        [Display(Name = "ภาพประกอบ")]
        [Required(ErrorMessage = "กรุณาเลือกภาพประกอบ")]
        public virtual string PreviewImageUrl { get; set; }


        public virtual DateTime UtcReleaseDate { get; set; }

        [Required]
        public virtual bool IsPublish { get; set; }

        public virtual int Hits { get; set; }
        public virtual IList<Tag> Tags { get; set; }

        //[Display(Name="หมวดหมู่ เว้นวรรคสำหรับหลายหมวดหมู่")]
        ////[Required(ErrorMessage="กรุณากรอกอย่างน้อยหนึ่งหมวดหมู่")]
        //public virtual string TagValue { get; set; }

        //comment list
        public virtual IList<Comment> Comments { get; set; }
        public virtual DateTime UtcCreateDate { get; set; }
        public virtual DateTime? UtcLastUpdate { get; set; }

        public Article()
        {
            IsPublish = true;
            Tags = new List<Tag>();
            Comments = new List<Comment>();
        }

        public override bool Equals(object obj)
        {
            var other = obj as Article;

            if (ReferenceEquals(null, other)) return false;

            if (ReferenceEquals(this, other)) return true;

            return Id == other.Id
                && Title == other.Title;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = GetType().GetHashCode();
                hash = (hash * 31) ^ Id.GetHashCode();
                hash = (hash * 31) ^ Title.GetHashCode();

                return hash;
            }
        }

    }
}

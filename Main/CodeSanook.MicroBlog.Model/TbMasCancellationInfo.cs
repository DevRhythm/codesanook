﻿namespace CodeSanook.MicroBlog.Model
{

    public class TbMasCancellationInfoKey
    {
        public virtual int CancelId{ get; set; }
        public virtual byte CultureId { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as TbMasCancellationInfoKey;

            if (ReferenceEquals(null, other)) return false;

            if (ReferenceEquals(this, other)) return true;

            return CancelId == other.CancelId 
                && CultureId == other.CultureId;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = GetType().GetHashCode();
                hash = (hash * 31) ^ CancelId.GetHashCode();
                hash = (hash * 31) ^ CultureId.GetHashCode();

                return hash;
            }
        }
    }
    public class TbMasCancellationInfo
    {
        public virtual TbMasCancellationInfoKey Key { get; set; }

        public virtual TbMasCancellation Cancellation { get; set; }
        public virtual string CancellationReason { get; set; }



    }
}
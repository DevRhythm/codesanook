using System;

namespace CodeSanook.MicroBlog.Model
{
    public class UserProfile
    {
        public virtual Guid Id { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual User User { get; set; }
    }
}
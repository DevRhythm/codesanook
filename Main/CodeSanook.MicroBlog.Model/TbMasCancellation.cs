﻿using System.Collections.Generic;

namespace CodeSanook.MicroBlog.Model
{
    public class TbMasCancellation
    {
        public virtual int CancellationId { get; set; }
        public virtual byte CultureId { get; set; }


        public virtual int Condition1StartDay { get; set; }
        public virtual int Condition1EndDay { get; set; }
        public virtual int Condition2StartDay { get; set; }

        public virtual int Condition2EndDay { get; set; }
        public virtual int Condition1CalculateRateId { get; set; }
        public virtual int Condition2CalculateRateId { get; set; }

        public virtual ICollection<TbMasCancellationInfo> CancellationInfos { get; set; } 

        public TbMasCancellation()
        {
            CancellationInfos = new List<TbMasCancellationInfo>();
        }
    }
}
using System;
using System.Collections.Generic;
using CodeSanook.MicroBlog.Infrastructure;

namespace CodeSanook.MicroBlog.Model.RepositoryInterfaces
{
    public interface IUserRepository : IRepository<User, Guid>
    {
        User GetByCookieId(string cookieId);
        int GetRowCountUserWithEmailAndPassword(string email, string password);
        User GetByEmail(string email);
        void AddRole(Role role);
        Role GetRoleByName(string roleName);
        IList<Role> GetAllRoles();
        void RemoveRole(string roleName);
        IList<User> GetAllUserInRole(string roleName);
        IList<Role> GetAllRoleForUser(int userId);

        User FindByEmail(string email);
    }
}
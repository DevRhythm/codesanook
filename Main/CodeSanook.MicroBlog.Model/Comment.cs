﻿using System;
using System.ComponentModel.DataAnnotations;
using CodeSanook.MicroBlog.Infrastructure;

namespace CodeSanook.MicroBlog.Model
{
    public class Comment:IUtcCreateDate,IUtcLastUpdate
    {
        public virtual int Id { get; set; }
        public virtual Article Article { get; set; }
        public virtual string UserIp { get; set; }

        [Required(ErrorMessage = "กรุณากรอกชื่อ")]
        public virtual string UserName { get; set; }

        [Required(ErrorMessage = "กรุณากรอกอีเมล์")]
        //[Email(ErrorMessage="กรุณากรอกรูปแบบอีเมล์ให้ถูกต้อง")]
        public virtual string Email { get; set; }

        [Required(ErrorMessage = "กรุณากรอกรายละเอียด")]
        public virtual string Details { get; set; }
       
        public virtual DateTime CreationDate { get; set; }

        [Required(ErrorMessage = "กรุณากรอกตัวอักษรที่ปรากฏในรูป")]
        public virtual string CaptchaCode { get; set; }

        public virtual DateTime UtcCreateDate { get; set; }
        public virtual DateTime? UtcLastUpdate { get; set; }
    }
}
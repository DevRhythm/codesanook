﻿using System.ComponentModel;

namespace CodeSanook.MicroBlog.Business.Messages.Requests
{
    public class ContactUsSaveRequest
    {

        [DisplayName("หัวข้อ")]
        public string Title { get; set; }

        [DisplayName("ชื่อ")]
        public string UserName { get; set; }

        [DisplayName("อีเมล์")]
        public string Email { get; set; }

        [DisplayName("รายละเอียด")]
        public string Details { get; set; }

        [DisplayName("อักษรในรูป")]
        public string CaptchaCode { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace CodeSanook.MicroBlog.Business.Messages.Requests
{
    public class UserLogInRquest
    {
        [Display(Name="อีเมล์")]
        [Required(ErrorMessage="กรุณากรอกอีเมล์")]
        public string Email { get; set; }

        [Display(Name = "รหัสผ่าน")]
        [Required(ErrorMessage="กรุณากรอกรหัสผ่าน")]
        public string Password { get; set; }

        public bool Remember { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using CodeSanook.MicroBlog.Business.Messages.Responses;
using CodeSanook.MicroBlog.Infrastructure;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using log4net;

namespace CodeSanook.MicroBlog.Business.Services
{
    public class ArticleService
    {
        public ILog Log { get; set; }
        public IArticleRepository ArticleRepository { get; set; }
        public IUnitOfWorkFactory UnitOfWorkFactory { get; set; }

        public ServiceResponse<ArticleListContent> GetArticleList(int pageNo, int itemPerPage)
        {
            var index = pageNo - 1;
            IList<Article> articleList;
            Log.DebugFormat("index: {0}, itemPerPage: {1}", index, itemPerPage);

            using (UnitOfWorkFactory.Create())
            {
                articleList = ArticleRepository.FindAll(index, itemPerPage);
            }
            Log.DebugFormat("aricleList count: {0}", articleList.Count());

            var numberOfArticles = ArticleRepository.GetNumberOfArticles();
            var responseData = new ArticleListContent()
            {
                ArticleList = articleList.ToList(),
                NumberOfArticles = numberOfArticles
            };
            var response = new ServiceResponse<ArticleListContent>(responseData);
            return response;
        }

        public ServiceResponse<Article> GetArticleDetail(string alias)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var article = ArticleRepository.GetByAlias(alias);
                    ////update hit
                    //article.TagValue = string.Join(" ", article.Tags.Select(t => t.Name).ToArray());
                    article.Hits++;
                    unitOfWork.Commit();
                    return new ServiceResponse<Article>(article);
                }
                catch (Exception ex)
                {
                    return new ServiceResponse<Article>(ex);
                }
            }
        }
        public ServiceResponse<Article> GetArticleDetail(int articleId)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var article = ArticleRepository.FindById(articleId);
                    ////update hit
                    //article.TagValue = string.Join(" ", article.Tags.Select(t => t.Name).ToArray());
                    article.Hits++;
                    unitOfWork.Commit();
                    return new ServiceResponse<Article>(article);
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    return new ServiceResponse<Article>(ex);
                }
            }
        }

        public ServiceResponse CreateArticle(Article article)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    //check publish now
                    //if (article.IsReleaseNow)
                    //{
                    //    article.ReleaseDate = DateTime.Now;
                    //}

                    //decorate alias for url friendly
                    if (!string.IsNullOrEmpty(article.Alias))
                    {
                        article.Alias = article.Alias.Trim().Replace(' ', '-').Replace('.', '-');
                    }
                    ArticleRepository.Add(article);

                    ////add tags
                    //if (!string.IsNullOrEmpty(article.TagValue))
                    //{
                    //    string[] tagNames = article.TagValue.Split(' ')
                    //        .Select(t => t.Trim().Replace('.', '-'))
                    //        .Where(t => t.Length > 0).ToArray();

                    //    foreach (var tagName in tagNames)
                    //    {
                    //        //create new tag if not exist 
                    //        var tag = ArticleRepository.GetTagByName(tagName);
                    //        //new tag
                    //        if (tag == null)
                    //        {
                    //            //add new tag
                    //            tag = new Tag() { Name = tagName };
                    //        }
                    //        //add tag to article
                    //        article.Tags.Add(tag);
                    //        tag.Articles.Add(article);
                    //    }
                    //}

                    unitOfWork.Commit();
                    var result = new ServiceResponse();
                    return result;
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    var result = new ServiceResponse(ex);
                    return result;
                }
            }
        }//end method

        public ServiceResponse EditArticle(int articleId)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var article = ArticleRepository.FindById(articleId);

                    if (article != null)
                    {
                        //article.TagValue = string.Join(" ", article.Tags.Select(t => t.Name).ToArray());
                        //decorate alias for url friendly
                        article.Alias = article.Alias.Replace('-', ' ');
                        ////adjust publish to show release date
                        //article.IsReleaseNow = false;
                    }

                    unitOfWork.Commit();
                    return new ServiceResponse();
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    return new ServiceResponse(ex);
                }
            }
        }//end method

        public ServiceResponse EditArticle(int articleId, Article articleEditRequest)
        {

            try
            {
                var article = ArticleRepository.FindById(articleId);
                if (article != null)
                {
                    article.Title = article.Title;
                    //decorate alias for url friendly
                    article.Alias = article.Alias.Trim().Replace(' ', '-').Replace('.', '-');
                    article.Details = article.Details;
                    article.Abstract = article.Abstract;
                    article.UtcLastUpdate = DateTime.Now;
                    article.PreviewImageUrl = article.PreviewImageUrl;

                    ///check publish now
                    // article.ReleaseDate = article.IsReleaseNow ? DateTime.Now : article.ReleaseDate;

                    //add tags
                    //add tags
                    //if (!string.IsNullOrEmpty(article.TagValue))
                    //{
                    //    string[] tagNames =
                    //        article.TagValue
                    //            .Split(' ')
                    //            .Select(t => t.Trim().Replace('.', '-'))
                    //            .Where(t => t.Length > 0).ToArray();

                    //    if (tagNames.Any())
                    //    {
                    //        //remove all tag to article
                    //        article.Tags.Clear();
                    //        //db.SaveChanges();
                    //        Tag tag;
                    //        foreach (var tagName in tagNames)
                    //        {

                    //            //create new tag if not exist 
                    //            tag = ArticleRepository.GetTagByName(tagName);
                    //            if (tag == null)
                    //            {
                    //                //add new tag
                    //                tag = new Tag() { Name = tagName };
                    //                ArticleRepository.AddTag(tag);
                    //            }
                    //            //add tag to article
                    //            article.Tags.Add(tag);
                    //        }
                    //    }

                    //}
                }

                var response = new ServiceResponse();
                return response;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                var response = new ServiceResponse(ex);
                return response;
            }

        }//end method

        public ServiceResponse DeleteArticle(int id)
        {

            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var article = ArticleRepository.FindById(id);
                    if (article != null)
                    {
                        //delete all comment cascade
                        ArticleRepository.Remove(article);

                        unitOfWork.Commit();
                    }

                    //delete image
                    if (article != null && HttpContext.Current != null)
                    {
                        var imagePath = HttpContext.Current.Server.MapPath("~" + article.PreviewImageUrl);
                        var fileInfo = new FileInfo(imagePath);
                        if (fileInfo.Exists)
                        {
                            fileInfo.Delete();
                        }
                        else
                        {
                            Log.ErrorFormat("no preview image path: {0} to delete", imagePath);
                        }
                    }

                    return new ServiceResponse();

                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    return new ServiceResponse(ex);

                }
            }
        }//end method

        public ServiceResponse TogglePublishArticle(int articleId)
        {
            using (var unitOfWork = UnitOfWorkFactory.Create())
            {
                try
                {
                    var article = ArticleRepository.FindById(articleId);
                    if (article != null)
                    {
                        //toggle publish
                        article.IsPublish = !article.IsPublish;
                    }

                    unitOfWork.Commit();
                    return new ServiceResponse();
                }
                catch (Exception ex)
                {
                    Log.Error(ex);
                    return new ServiceResponse(ex);
                }
            }
        }

    }//end class

}
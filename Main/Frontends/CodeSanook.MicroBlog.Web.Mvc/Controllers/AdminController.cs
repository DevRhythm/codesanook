﻿using System.Web.Mvc;

namespace CodeSanook.MicroBlog.Web.Mvc.Controllers
{
    public class AdminController : Controller
    {
        
      
        [CodeSanookAuthorize(Roles="admin")]
        public ActionResult Index()
        {
            return View();
        }

       
    }
}

﻿using System.Web.Mvc;
using CodeSanook.MicroBlog.Business.Services;
using CodeSanook.MicroBlog.Model;
using CodeSanook.Utility;

namespace CodeSanook.MicroBlog.Web.Mvc.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /Member/

        public UserService UserService { get; set; }

        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Member/Create

        [HttpPost]
        public ActionResult Register(User user)
        {
            try
            {
                //register new member
                UserService.Register(user);
                return RedirectToAction("index","admin");
            }
            catch
            {
                return View();
            }
        }
        

        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(string email, string password,bool isRemember)
        {
            if(UserService.ValidateUserLogIn(email,password))
            {
                //get member 
                UserService.LogIn(email, isRemember);
                return RedirectToAction("index", "admin");
            }
            ModelState.AddModelError("invalidLogon", "Email or Password incorrect");
            return View();

        }

        public ActionResult LogOut()
        {
            UserService.LogOut();
            return RedirectToAction("index", "article");
        }

        public ActionResult HashPasword(string p)
        {
            return Content(HashHelper.GetHash(p, HashHelper.HashType.SHA256));
        }
    }

}

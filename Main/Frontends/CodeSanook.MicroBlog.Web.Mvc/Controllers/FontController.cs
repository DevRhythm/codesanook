﻿using System.Collections;
using System.Web.Mvc;

namespace CodeSanook.MicroBlog.Web.Mvc.Controllers
{
    public class FontController : Controller
    {
        //
        // GET: /Font/

        public ActionResult Index()
        {
            return View();
        }

        //
        public ActionResult Family(string fontName)
        {
            if (string.IsNullOrEmpty(fontName))
            {
                return Content("please provide font name");
            }
            var url = Request.Url;
           // var fontPath = Server.MapPath(string.Format("~/Content/Fonts/{0}.eot", fontName));
            //font url 
            var schema = Request.Url.Scheme;
            var host = Request.Url.Host;
            var port = Request.Url.Port;

            var fontUrl =
                string.Format(@"{0}://{1}:{2}/Content/Fonts/{3}", schema, host, port, fontName);
            //var fontUrl =
            //    string.Format(@"{0}/Content/Fonts/{1}",Request.Url.Authority, fontName);

            //render css content
            var templateVars = new Hashtable();
            templateVars.Add("fontName", fontName);
            templateVars.Add("fontUrl", fontUrl);

            var parser =
                        new Parser(Server.MapPath("~/Content/Fonts/FontFace.htm"), templateVars);

            var cssContent = parser.Parse();

            return Content(cssContent, "text/css");
        }

    }
}

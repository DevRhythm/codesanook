﻿using System;
using System.Collections;
using System.Web.Mvc;
using CodeSanook.MicroBlog.Web.Mvc.ViewModels;

namespace CodeSanook.MicroBlog.Web.Mvc.Controllers
{
    public class AboutController:Controller
    {
        //
        // GET: /About/
        private readonly string sessionKey = "captcha";
        public ActionResult Index()
        {
            if (TempData["message"] != null)
            {
                var message = TempData["message"] as string;
                if (!string.IsNullOrEmpty(message))
                {
                   ViewBag.Message = message;
                }
            }

            return View();
        }




        [HttpPost]
        public ActionResult Index(ContactUs model)
        {
            if (ValidCaptcha(model.CaptchaCode) && ModelState.IsValid)
            {
                //send mail
                var templateVars = new Hashtable();
                templateVars.Add("userName", model.UserName);
                templateVars.Add("email", model.Email);
                templateVars.Add("details", model.Details);

                var parser =
                    new Parser(Server.MapPath("~/MailTemplates/ContactUs.htm"), templateVars);

                var mailService = new MailService("admin@codesanook.com");
                mailService.Subject = model.Title;
                mailService.Body = parser.Parse();

                try
                {
                    mailService.Send();
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("", e.Message);
                }

                TempData["message"] = "อีเมล์ได้ถูกส่งไปแล้ว";
                return RedirectToAction("index");

            }

            //reset captcha
            model.CaptchaCode = null;
            return View(model);
        }

        /// <summary>
        /// render captcha image
        /// </summary>
        /// <returns>captcha image</returns>
        public ActionResult RenderCaptcha()
        {
            var result = CaptchaHelper.GetCaptcha();
            if (Session[sessionKey] == null)
            {
                //create session 
                Session.Add(sessionKey, result.Solution);
            }
            //use exist one
            Session[sessionKey] = result.Solution;

            return new FileContentResult(result.Image, result.ContentType);
        }

        /// <summary>
        /// valid captcha user type is correct
        /// </summary>
        /// <param name="captchaCode">captcha from user</param>
        /// <returns>true if correct</returns>
        public bool ValidCaptcha(string captchaCode)
        {
            if (string.IsNullOrEmpty(captchaCode) || Session[sessionKey] == null)
            {
               // ModelState.AddModelError("CaptchaCode", "กรุณากรอกตัวอักษรที่ปรากฏในรูปภาพ");
                return false;
            }

            var code = (string)Session[sessionKey];

            bool result = string.Compare(code.ToLower(), captchaCode.ToLower()) == 0;

            if (!result)
            {
                ModelState.AddModelError("CaptchaCode", "กรุณากรอกตัวอักษรที่ปรากฏในรูปให้ถูกต้อง");
            }

            return result;
        }

    }
}



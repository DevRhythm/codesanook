using System;

namespace CodeSanook.MicroBlog.Web.Mvc.Controllers
{
    public class CodeSanookAuthorizeAttribute : Attribute
    {
        public string Roles { get; set; }
    }
}
﻿using System.Web.Mvc;
using System.Web.Routing;

namespace CodeSanook.MicroBlog.Web.Mvc.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "ArticleDetails",
                url: "article/details/{articleId}/{alias}",
                defaults: new { controller = "Article", action = "Details"},
                constraints:    new { articleId = @"\d+" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Article", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
﻿(function($) {

    $.sarapadchang = {
        parentChildSelectList: function(options) {


            $("#" + options.parentId).find("option").click(function() {
                $("#" + options.childId).empty(); //clear data
                $("#" + options.childId).append('<option>loading...</option>');

                $.post("/" + options.controllerName + "/" + options.actionName + "/" + $(this).attr('value'), "", function(data) {


                    var html = "";

                    $.each(data, function(index, entry) {
                        html +=
               '<option value="' +
                entry['Value'] +
                '">' +
                entry['Text'] +
                '</option>';

                    }
                );

                    $("#" + options.childId).empty()
                    $("#" + options.childId).append(html);
                }, "json"); //end getJson
            });



        }, //end parentChildSelectList function

        rating: function() {
            $(".rating input").hover(

               function() {
                   $(this).parent().css("cursor", "pointer");
                   giveRating($(this), "FilledStar.png"); //.css("cursor", "pointer");

               },
               function() {
                   giveRating($(this), "EmptyStar.png");
               }
               );


            $(".rating input").click(function() {
                var parent = $(this).parent();
                parent.find("input").unbind("mouseout mouseover click")
                .css("cursor", "default").end().css("cursor", "default"); ;
                // call ajax methods to update database

                var url = "/" + parent.find(".controllerName").attr("value") + "/" +
                 parent.find(".actionName").attr("value") + "?rating=" + $(this).attr("value")
                + "&id=" + parent.find(":hidden").attr("value");
                $.post(url, null, function(data) {
                    alert(data);
                });
            });


            function giveRating(item, image) {
                item.attr("src", "/Content/Images/" + image)
               .prevAll("input").attr("src", "/Content/Images/" + image);
            }

        }, //end rating function;

        datePicker: function() {

            $.datepicker.regional['th'] = {
                closeText: 'ปิด',
                prevText: '&laquo;&nbsp;ย้อน',
                nextText: 'ถัดไป&nbsp;&raquo;',
                currentText: 'วันนี้',
                monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
		'กรกฏาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
		'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
                dayNamesShort: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
                dayNamesMin: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
                weekHeader: 'Wk',
                dateFormat: 'dd/mm/yy',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['th']);


            $(".datePicker").datepicker({

                dateFormat: 'd MM yy',
                showAnim: 'fadeIn',
                showButtonPanel: false,
                showOtherMonths: true,
                selectOtherMonths: true


            });


        }, //end datePicker function

        imageUploaderFor: function() {
            $(".imageUploader a").click(function(e) {
                var inputFileName = "";
                e.preventDefault();

                var imageUploader = $(this).parent();
                var imageName = imageUploader.find("img").attr("src").split("/");

                imageName = imageName[imageName.length - 1];
                var fileName = imageUploader.find("input").attr("name");
                imageUploader.empty();
                imageUploader.append('<input type="file" name="' + fileName + '"/>');
                var url = $(this).attr("href") + imageName;
       
                $.get(url, function(data) {
                    if (data == 'True') {   
                    }
                });
          
            });
        } //end imageUploaderFor




}//end sarapadchang namespace


$.fn.datePicker = function() {

            this.each(function() {

                $.datepicker.regional['th'] = {
                    closeText: 'ปิด',
                    prevText: '&laquo;&nbsp;ย้อน',
                    nextText: 'ถัดไป&nbsp;&raquo;',
                    currentText: 'วันนี้',
                    monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
		'กรกฏาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.',
		'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                    dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
                    dayNamesShort: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
                    dayNamesMin: ['อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'],
                    weekHeader: 'Wk',
                    dateFormat: 'dd/mm/yy',
                    firstDay: 0,
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: ''
                };
                $.datepicker.setDefaults($.datepicker.regional['th']);




                return $(this).datepicker({

                    dateFormat: 'd MM yy',
                    showAnim: 'fadeIn',
                    showButtonPanel: false,
                    showOtherMonths: true,
                    selectOtherMonths: true


                });


            }); //end each function

        } //end datePicker function



    })(jQuery);


 
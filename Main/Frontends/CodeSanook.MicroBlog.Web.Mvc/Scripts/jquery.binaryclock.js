$(document).ready(function() {
		updateBitCount();
		setInterval("updateBitCount()", 1000);
	});

	function checkBit( bitGroup , bitPlace, value)
	{
			if( (value & bitPlace ) == bitPlace )
			{
				$(bitGroup + bitPlace).addClass('count-block-active');
				value &= ~bitPlace;
			}
			else
			{
				$( bitGroup + bitPlace ).removeClass('count-block-active');
			}

	}

	function updateBitCount()
	{
		var current=new Date();
		var secDigit0 = current.getSeconds() % 10;
		var secDigit1 = (current.getSeconds() - secDigit0 ) / 10 ;
		var minDigit0 = current.getMinutes() % 10;
		var minDigit1 = (current.getMinutes() - minDigit0 ) / 10 ;
		var hrDigit0 = current.getHours() % 10;
		var hrDigit1 = (current.getHours() - hrDigit0 ) / 10 ;

		for( var i=8; i >= 1; i /= 2 )
		{
			checkBit( '#s0c' , i, secDigit0);
			checkBit( '#s1c' , i, secDigit1);
			checkBit( '#m0c' , i, minDigit0);
			checkBit( '#m1c' , i, minDigit1);
			checkBit( '#h0c' , i, hrDigit0);
			checkBit( '#h1c' , i, hrDigit1);
		}
	}
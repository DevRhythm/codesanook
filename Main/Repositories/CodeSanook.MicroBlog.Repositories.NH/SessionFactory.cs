﻿using System.Configuration;
using CodeSanook.MicroBlog.Infrastructure.SessionStorage;
using CodeSanook.MicroBlog.Repositories.NH.Mappings;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Event;
using NHibernate.SqlAzure;
using Configuration = NHibernate.Cfg.Configuration;

namespace CodeSanook.MicroBlog.Repositories.NH
{
    //how to install NHibernate Reliable SQL Azure Driver
    //project site
    //https://github.com/MRCollective/NHibernate.SqlAzure

    //Update-Package FluentNHibernate
    //Install-Package NHibernate.SqlAzure
    public class SessionFactory
    {
        private static ISessionFactory NhSessionFactory;
        private static Configuration config;

        private static  readonly string _connectionString = ConfigurationManager.AppSettings["connectionString"];
        public static Configuration Config
        {
            get { return config; }
        }

        public static void Initialize()
        {
            config = Fluently.Configure()
                    .Database(GetDatabaseConfig())
                    .Mappings(m =>
                                  {
                                      m.FluentMappings.AddFromAssemblyOf<UserMap>();

                                      ////to use name query in xml file
                                      m.HbmMappings.AddFromAssemblyOf<UserMap>();
                                  })
                    .ExposeConfiguration(SetupAdditionalConfiguration)
                    .BuildConfiguration(); 

            NhSessionFactory = config.BuildSessionFactory();
        }

        private static IPersistenceConfigurer GetDatabaseConfig()
        {
            var databaseConfig = MsSqlConfiguration.MsSql2008
                .ShowSql()
                .FormatSql()
                .ConnectionString(_connectionString)
                //setup driver for retry connection
                .Driver<SqlAzureClientDriver>();
            return databaseConfig;
        }


        private static void SetupAdditionalConfiguration(Configuration cfg)
        {
            //config.SetProperty("current_session_context_class", "web");
            //support transaction
            cfg.SetProperty(Environment.TransactionStrategy,
                typeof(ReliableAdoNetWithDistributedTransactionFactory).AssemblyQualifiedName);

            cfg.EventListeners.PreInsertEventListeners
            = new IPreInsertEventListener[]
                                                 {
                                                    new AuditEventListener()
                                                 };

            cfg.EventListeners.PreUpdateEventListeners

                 = new IPreUpdateEventListener[]
                                                 {
                                                    new AuditEventListener()
                                                 };

            //for unique constrain exception
            cfg.SetProperty("sql_exception_converter",
                typeof(SqlServerExceptionConverter).AssemblyQualifiedName);
        }


        public static ISessionFactory GetSessionFactory()
        {
            if (NhSessionFactory == null)
                Initialize();//we can move this to Global.ascx
            return NhSessionFactory;
        }

        private static ISession GetNewSession()
        {
            return GetSessionFactory().OpenSession();
        }

        public static ISession GetCurrentSession()
        {
            var sessionStorageContainer = SessionStorageFactory.GetStorageContainer();

            var currentSession = sessionStorageContainer.GetCurrentSession();
            if (currentSession == null)
            {
                currentSession = GetNewSession();
                sessionStorageContainer.Store(currentSession);
            }
            return currentSession;
        }

        public static void ClearCurrentSession()
        {
            var sessionStorageContainer = 
                SessionStorageFactory .GetStorageContainer();
            sessionStorageContainer.Clear();
        }

    }//end class
}

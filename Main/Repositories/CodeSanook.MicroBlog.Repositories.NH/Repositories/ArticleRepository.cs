﻿using System.Collections.Generic;
using System.Linq;
using CodeSanook.MicroBlog.Model;
using CodeSanook.MicroBlog.Model.RepositoryInterfaces;
using NHibernate.Linq;

namespace CodeSanook.MicroBlog.Repositories.NH.Repositories
{
    public class ArticleRepository:RepositoryBase<Article,int>,IArticleRepository
    {
        public Article GetByAlias(string alias)
        {
            var session = SessionFactory.GetCurrentSession();
            var article =  session.Query<Article>()
                .Single(a => a.Alias == alias);
            return article;
        }

        public Tag GetTagByName(string tagName)
        {
            throw new System.NotImplementedException();
        }

        public void AddTag(Tag tag)
        {
            throw new System.NotImplementedException();
        }

        public int GetNumberOfArticles()
        {
            var session = SessionFactory.GetCurrentSession();
            return session.Query<Article>().Count();
        }
    }
}
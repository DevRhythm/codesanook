﻿using System;
using System.ComponentModel.DataAnnotations;
using CodeSanook.MicroBlog.Model;
using FluentNHibernate.Mapping;

namespace CodeSanook.MicroBlog.Repositories.NH.Mappings
{
    public class CommentMap : ClassMap<Comment>
    {
        public CommentMap()
        {
            Table("Comments");
            Id(x => x.Id).GeneratedBy.Identity();
            //References(x => x.Article).ForeignKey("none").Column("ArticleId");
            Map(x => x.UserIp).Length(255).Not.Nullable();

            Map(x => x.UserName).Length(255).Not.Nullable();
            Map(x => x.Email).Length(255).Not.Nullable();
            Map(x => x.Details).Length(10000).Not.Nullable();

            Map(x => x.UtcCreateDate).Not.Nullable();
            Map(x => x.UtcLastUpdate);
        }
    }
}
﻿using CodeSanook.MicroBlog.Model;
using FluentNHibernate.Mapping;

namespace CodeSanook.MicroBlog.Repositories.NH.Mappings
{
    public class TbMasCancellationMap : ClassMap<TbMasCancellation>
    {
        public TbMasCancellationMap()
        {
            Id(x => x.CancellationId).GeneratedBy.Identity();
            Map(x => x.CultureId);

            Map(x => x.Condition1StartDay); 
            Map(x => x.Condition2StartDay); 
            Map(x => x.Condition2CalculateRateId);

            Map(x => x.Condition1EndDay); 
            Map(x => x.Condition2EndDay); 
            Map(x => x.Condition1CalculateRateId);

            HasMany(x => x.CancellationInfos).KeyColumn("CancellationId").ForeignKeyConstraintName("none");
        }
    }
}
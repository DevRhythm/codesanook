﻿using System;

namespace CodeSanook.MicroBlog.Repositories.NH
{
    public class UniqueKeyException : Exception
    {
        public UniqueKeyException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }

}

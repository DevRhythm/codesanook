﻿using CodeSanook.MicroBlog.Infrastructure;

namespace CodeSanook.MicroBlog.Repositories.NH
{
    public class NHUnitOfWorkFactory : IUnitOfWorkFactory
    {
        public IUnitOfWork Create()
        {
            return Create(false);
        }

        public IUnitOfWork Create(bool forceNew)
        {
            return new NHUnitOfWork(forceNew);
        }
    }
}

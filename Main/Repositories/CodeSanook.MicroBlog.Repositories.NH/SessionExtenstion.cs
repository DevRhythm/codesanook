﻿using System.IO;
using System.Text.RegularExpressions;
using NHibernate;
using log4net;

namespace CodeSanook.MicroBlog.Repositories.NH
{
   public static class SqlHelper
    {
       static ILog _log = LogManager.GetLogger(typeof(SqlHelper).Namespace);

       public static void ExecuteSqlFile(this ISession session, string sqlFilePath)
       {
           string sql;
           using (var fileStream = File.OpenRead(sqlFilePath))
           {
               var reader = new StreamReader(fileStream);
               sql = reader.ReadToEnd();
           }

           var regex = new Regex("^GO", RegexOptions.IgnoreCase | RegexOptions.Multiline);
           var lines = regex.Split(sql);
           foreach (string line in lines)
           {
               _log.DebugFormat("line = {0}", line);
               if (!string.IsNullOrEmpty(line))
               {
                   IQuery query = session.CreateSQLQuery(line);
                   query.ExecuteUpdate();
               }
           }

       }//end method

    }//end class
}

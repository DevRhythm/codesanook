﻿using System;

namespace CodeSanook.MicroBlog.Infrastructure
{
    public interface IUnitOfWork:IDisposable
    {
        void Commit(bool isWithTransactionRollback);
        void Commit();

        void Undo();
    }
}
﻿using System;

namespace CodeSanook.MicroBlog.Infrastructure
{
    public interface IUtcLastUpdate
    {
        DateTime? UtcLastUpdate { get; set; }
    }
}
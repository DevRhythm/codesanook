﻿namespace CodeSanook.MicroBlog.Infrastructure
{
public   interface IUnitOfWorkFactory
    {
        IUnitOfWork Create();
        IUnitOfWork Create(bool forceNew);
    }
}

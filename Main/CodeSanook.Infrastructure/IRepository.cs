﻿using System.Collections;
using System.Collections.Generic;

namespace CodeSanook.MicroBlog.Infrastructure
{
    public interface IRepository<T, K> where T : IAggregateRoot
    {
        void Add(T entity);
        void Remove(T entity);
        T FindById(K Id);
        IList<T> FindAll();
        IList<T> FindAll(int index, int count);

        T1 GetBy<T1>(IDictionary<string, object> keyValues) where T1 : class;
        T1 GetBy<T1>(params object[] keyValue) where T1 : class;
    }

}

﻿using NHibernate;

namespace CodeSanook.MicroBlog.Infrastructure.SessionStorage
{
    public interface ISessionStorageContainer
    {
        ISession GetCurrentSession();
        void Store(ISession session);
        void Clear();
    }
}

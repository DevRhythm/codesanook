﻿using System;
using System.Collections;
using System.Threading;
using NHibernate;
using log4net;

namespace CodeSanook.MicroBlog.Infrastructure.SessionStorage
{
    //when start new service start with new Thread
    public class ThreadSessionStorageContainer : ISessionStorageContainer 
    {
        private static readonly Hashtable _sessions = new Hashtable();
        private ILog _log = LogManager.GetLogger("Noonswoon.Infrastructure");

        public ISession GetCurrentSession()
        {
            ISession nhSession = null;
            var threadName = GetThreadName();
            //_log.DebugFormat("getting current session from thread name: {0}", threadName);
            if (_sessions.Contains(threadName))
            {
                nhSession = (ISession) _sessions[threadName];
            }
            return nhSession;
        }

        public void Store(ISession session)
        {
            if (_sessions.Contains(GetThreadName()))
            {
                _sessions[GetThreadName()] = session;
            }
            else
            {
                _sessions.Add(GetThreadName(), session);
            }
        }

        public void Clear()
        {
            var session = GetCurrentSession();
            if (session != null)
            {
                _sessions[GetThreadName()] = null;
                session.Dispose();
            }
        }

        private static string GetThreadName()
        {
            if (string.IsNullOrEmpty(Thread.CurrentThread.Name))
            {
                Thread.CurrentThread.Name = Guid.NewGuid().ToString();//set thread name
            }
            return Thread.CurrentThread.Name;
        }
    }
}

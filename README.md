This is a simple webblog engine that try to make as example for N-Layer application and host my personal article.

The project consist of technology for example

 - NHibernate with FluentNHibernate
 - NAnt
 - Log4Net
 - NUnit
 - Autofac
 - Moq
 - Support connect with SQL Azure
 - and implement some design pattern
 - Unit Of Work
 - Inversion of Control
 - Repository
 - Builder


How to run Integration Test Project
Open file
**Main\CodeSanook.MicroBlog.Configuration\Environments\VisualStudioConfigurations\Debug.properties**
and change to your connection string SQL server 2008 up or SQL Azure

    <project>
    <property name="connectionString" value="your-connection-string-to-development-server"/>
    <property name="log4netDebug" value="true"/>
</project>

and run Test Fxture class 
**Main\Tests\CodeSanook.MicroBlog.Tests.Integration\ArticleServiceTest.cs**
I recommend Resharper Test runner since it very easy to run test case

Please make sure you are in Visual Studio Debug configuration.
You will get the result like this if you set everything correctly.



If you want to connect to production database or other database, you don't need to change connection string manually. Just set connection value in file
**Main\CodeSanook.MicroBlog.Configuration\Environments\VisualStudioConfigurations\Release.properties**
as following

    <project>
    <property name="connectionString" value="your-connection-string-to-production-server"/>
    <property name="log4netDebug" value="false"/>
</project>

If you have any question post in our community web
[ASP.NET & MVC Developers Thailand][1]

I hope this project can help other people who want to create project that can testable and show how to use **NHibernate** in Real world scenario



credits:
[Chalermpon Areepong] [2]


NHibernate tools  
[http://nmg.codeplex.com/](http://nmg.codeplex.com/)

  [1]: https://www.facebook.com/groups/MVCTHAIDEV/
  [2]: https://www.facebook.com/nine.mvp
  
  
  
  
  
